const router = require('express').Router() //el método Router permite tener un objeto que facilita la creación de rutas
const { getPets, getPet, getUserPets, postNewPet, changePetValues, changeStateOfPetToFound, deletePet } = require('../../controller/petController')

router.get('/api/pet', getPets)

router.get('/api/pet/:_id', getPet)

router.get('/api/user/get-pets/:_id', getUserPets)

router.post('/api/pet/create-pet', postNewPet)

router.put('/api/pet/update-pet/:_id', changePetValues)

router.put('/api/pet/update-pet-found/:_id', changeStateOfPetToFound)

router.delete('/api/pet/delete-pet/:_id', deletePet)


module.exports = router