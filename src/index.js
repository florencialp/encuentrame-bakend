require('dotenv').config()
const express = require('express')
const cors = require('cors')

// initializations
const app = express()
require('./database')

// settings
app.set('port', process.env.PORT || 3000) //se configura el puerto

// middlewares
app.use(cors())
app.use(express.urlencoded({ extended: false })) //express.urlencoded() sirve para entender la info de un formulario
app.use(express.json())
app.use(express.urlencoded({ extended: false })) //para entender que datos está enviando el formulario

// routes
const apiRoute = require('./api/routes/routes')
app.use('/', apiRoute)

// server
app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'))
})